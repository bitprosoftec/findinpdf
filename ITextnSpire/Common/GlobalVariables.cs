﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ITextnSpire.Common
{
    public static class GlobalVariables
    {
        public const string PDF = "PDF";
        public const string SplittedPDF = "SplittedPDF";
        public const string SplittedHighlightedPDFS = "SplittedHighlightedPDFS";
        public const string PdfDirectory = "PdfStorage";
        public const string UploadedPdf = "UploadedPdf";
    }
}
