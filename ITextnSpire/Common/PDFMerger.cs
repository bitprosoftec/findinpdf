﻿using iText.Kernel.Pdf;
using iText.Kernel.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ITextnSpire.Models;

namespace ITextnSpire.Common
{
    public static class PDFMerger
    {
        public static string Merge(int pageparts, PDFFilenText pf, string PDFGeneralPath)
        {
            var filename = pf.PDFFIle.FileName;
            string HighlightDownload = PDFGeneralPath + "\\Highlighted_" + filename;
            string SplittedSRC = string.Empty;

            PdfDocument pdf = new PdfDocument(new PdfWriter(HighlightDownload));
            
            for (int i = 1; i <= pageparts; i++)
            {
                PdfMerger merger = new PdfMerger(pdf);
                //Add pages from the first document
                SplittedSRC = PDFGeneralPath + "\\" + Common.GlobalVariables.SplittedHighlightedPDFS + "\\" + "splitDocument_" + i + ".pdf"; 

                PdfDocument SourcePdf = new PdfDocument(new PdfReader(SplittedSRC));
                merger.Merge(SourcePdf, 1, SourcePdf.GetNumberOfPages());
                SourcePdf.Close();
                SplittedSRC = String.Empty;
            }

            pdf.Close();
            return HighlightDownload;
        }
    }
}
