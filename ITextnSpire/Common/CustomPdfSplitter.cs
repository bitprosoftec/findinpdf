﻿using iText.Kernel.Pdf;
using iText.Kernel.Utils;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ITextnSpire.Common
{
    public class CustomPdfSplitter : PdfSplitter
    {
        int _partNumber = 1;
        private readonly IWebHostEnvironment _hostingEnvironment;

        public CustomPdfSplitter(PdfDocument pdfDocument, IWebHostEnvironment hostingEnvironment) : base(pdfDocument)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        protected override PdfWriter GetNextPdfWriter(PageRange documentPageRange)
        {
            string path = string.Empty;
            path = Path.Combine(_hostingEnvironment.WebRootPath, Common.GlobalVariables.PDF + "\\", Common.GlobalVariables.SplittedPDF + "\\", "splitDocument_" + _partNumber++ + ".pdf");
            
            try
            {
                return new PdfWriter(path);
            }
            catch (FileNotFoundException e)
            {
                throw new SystemException();
            }
        }
    }
}
