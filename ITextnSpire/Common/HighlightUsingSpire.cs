﻿using Spire.Pdf;
using Spire.Pdf.General.Find;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ITextnSpire.Common
{
    public static class HighlightUsingSpire
    {
        public static void HighlightPdf(int parts, string searchText, string PDFGeneralPath, string filename, string PDFGeneralPathToPDFStorage)
        {
            int newNames = 1;
            string SavingHighlighted = String.Empty;
            string src = String.Empty;
            if (parts == 0)
            {
                src = PDFGeneralPathToPDFStorage + "\\" + filename;
                PdfDocument pdf = new PdfDocument(src);

                PdfTextFind[] result = null;
                for (int j = 0; j < pdf.Pages.Count; j++)
                {
                    result = pdf.Pages[j].FindText(searchText, TextFindParameter.CrossLine).Finds;
                    foreach (PdfTextFind find in result)
                    {
                        find.ApplyHighLight(System.Drawing.Color.Yellow);
                    }
                }
                string HighlightDownload = PDFGeneralPath + "\\Highlighted_" + filename;
                pdf.SaveToFile(HighlightDownload, FileFormat.PDF);
                pdf.Close();
            }
            else
            {
                for (int i = 1; i <= parts; i++)
                {
                    src = PDFGeneralPath + "\\" + Common.GlobalVariables.SplittedPDF + "\\" + "splitDocument_" + i + ".pdf";     // + "\\" + "splitDocument_" + i + ".pdf"
                    PdfDocument pdf = new PdfDocument(src);

                    PdfTextFind[] result = null;
                    for (int j = 0; j < pdf.Pages.Count; j++)
                    {
                        result = pdf.Pages[j].FindText(searchText, TextFindParameter.CrossLine).Finds;
                        foreach (PdfTextFind find in result)
                        {
                            find.ApplyHighLight(System.Drawing.Color.Yellow);
                        }
                    }
                    SavingHighlighted = PDFGeneralPath + "\\" + Common.GlobalVariables.SplittedHighlightedPDFS + "\\" + "splitDocument_" + newNames + ".pdf";     // + "\\" + "splitDocument_" + i + ".pdf"

                    pdf.SaveToFile(SavingHighlighted, FileFormat.PDF);
                    newNames++;
                    pdf.Close();
                    SavingHighlighted = String.Empty;
                    src = String.Empty;
                }
            }
            
        }
    }
}
