﻿using iText.Kernel.Pdf;
using ITextnSpire.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using ITextnSpire.Common;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;

namespace ITextnSpire.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IWebHostEnvironment _hostingEnvironment;
        
        public HomeController(ILogger<HomeController> logger, IWebHostEnvironment hostingEnvironment)
        {
            _logger = logger;
            _hostingEnvironment = hostingEnvironment;
        }

        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Index(PDFFilenText pf)
        {
            string path = string.Empty;
            string PDFGeneralPath = Path.Combine(_hostingEnvironment.WebRootPath, Common.GlobalVariables.PDF);
            string PDFGeneralPathToPDFStorage = Path.Combine(_hostingEnvironment.WebRootPath, Common.GlobalVariables.PdfDirectory);
            string newPdf = String.Empty;
            DeleteObsoleteFiles(PDFGeneralPath);
            bool FileUploaded = await UploadPDF(pf);
            var filename = pf.PDFFIle.FileName;
            if (FileUploaded == true)
            {
                path = Path.Combine(_hostingEnvironment.WebRootPath, Common.GlobalVariables.PdfDirectory + "\\" , pf.PDFFIle.FileName);

                PdfDocument pdfDoc = new PdfDocument(new PdfReader(path));
                int maxPageCount = 9; // create a new PDF per 2 pages from the original file
                int numberOfPages = pdfDoc.GetNumberOfPages();
                int pageparts = 0;
                if (numberOfPages >= 9)
                {
                    // Splitting PDF
                    IList<PdfDocument> splitDocuments = new CustomPdfSplitter(pdfDoc, _hostingEnvironment).SplitByPageCount(maxPageCount);

                    foreach (PdfDocument doc in splitDocuments)
                    {
                        pageparts++;
                        doc.Close();
                    }

                    pdfDoc.Close();
                    // Highlighting PDF
                    HighlightUsingSpire.HighlightPdf(pageparts, pf.SearchText, PDFGeneralPath, filename, PDFGeneralPathToPDFStorage);
                    //Merging Pdf
                    newPdf = PDFMerger.Merge(pageparts, pf, PDFGeneralPath);
                }
                else
                {
                    // Highlighting PDF
                    HighlightUsingSpire.HighlightPdf(pageparts, pf.SearchText, PDFGeneralPath, filename, PDFGeneralPathToPDFStorage);
                    newPdf = PDFGeneralPath + "\\Highlighted_" + filename;
                }
                // Delete Obsolete Pdfs
                DeleteObsoleteFiles(PDFGeneralPath);
                var p = new Process();
                p.StartInfo = new ProcessStartInfo(newPdf)
                {
                    UseShellExecute = true
                };
                p.Start();
            }
            
            return RedirectToAction("Index");
        }

        public void DeleteObsoleteFiles(string PDFGeneralPath)
        {
            string highlighted = PDFGeneralPath + "\\" + GlobalVariables.SplittedHighlightedPDFS ;
            string Splitted = PDFGeneralPath + "\\" + GlobalVariables.SplittedPDF;
            System.IO.DirectoryInfo di = new DirectoryInfo(highlighted);
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
            System.IO.DirectoryInfo di2 = new DirectoryInfo(Splitted);
            foreach (FileInfo file2 in di2.GetFiles())
            {
                file2.Delete();
            }
        }
        public IActionResult Privacy()
        {
            return View();
        }
        #region Upload File
        private async Task<bool> UploadPDF(PDFFilenText pf)
        {
            var result = false;
            var path = string.Empty;
            try
            {
                if ((pf.PDFFIle != null && pf.PDFFIle.Length > 0))
                {
                    path = Path.Combine(_hostingEnvironment.WebRootPath, Common.GlobalVariables.PdfDirectory);
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);
                }

                if (pf.PDFFIle != null && pf.PDFFIle.Length > 0)
                {

                    var type = pf.PDFFIle.ContentType.Split('/').LastOrDefault();
                    pf.PDFFileName = pf.PDFFIle.FileName;
                    //pf.PDFFIle.CopyTo(new FileStream(Path.Combine(path, pf.PDFFileName), FileMode.Create));
                    using (var stream = new FileStream(Path.Combine(path, pf.PDFFileName), FileMode.Create))
                    {
                        await pf.PDFFIle.CopyToAsync(stream);
                        //stream.CopyToAsync(stream);
                    }

                    result = true;
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
            }
            return result;
        }

        #endregion

        #region SplitPdf
        public IActionResult SplitPdf()
        {
            if (TempData["Paths"] != null) {
                var pdfpaths = TempData["Paths"] ;
                ViewBag.Paths = pdfpaths;
            }
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> SplitPdf(PDFFilenText pf)
        {
            string path = string.Empty;
            bool FileUploaded = await UploadPDF(pf);
            List<String> paths = new List<string>();
            string PDFGeneralPath = Path.Combine(_hostingEnvironment.WebRootPath, Common.GlobalVariables.PDF);
            DeleteObsoleteFiles(PDFGeneralPath);
            if (FileUploaded == true)
            {
                path = Path.Combine(_hostingEnvironment.WebRootPath, Common.GlobalVariables.PdfDirectory + "\\", pf.PDFFIle.FileName);

                PdfDocument pdfDoc = new PdfDocument(new PdfReader(path));
                int maxPageCount = pf.PageNumber;
                int numberOfPages = pdfDoc.GetNumberOfPages();
                if (numberOfPages >= maxPageCount)
                {
                    // Splitting PDF
                    IList<PdfDocument> splitDocuments = new CustomPdfSplitter(pdfDoc, _hostingEnvironment).SplitByPageCount(maxPageCount);
                    foreach (PdfDocument doc in splitDocuments)
                    {
                        doc.Close();
                    }
                    pdfDoc.Close();
                    
                    for (int i = 1; i<= splitDocuments.Count; i++)
                    {
                        string newPath = _hostingEnvironment.WebRootPath + "\\" + Common.GlobalVariables.PDF + "\\"+ Common.GlobalVariables.SplittedPDF + "\\"+ "splitDocument_" + i + ".pdf";
                        paths.Add(newPath);
                    }
                    TempData["Paths"] = paths;
                    //TempData.Keep("Paths");
                }
                
            }
            
            return RedirectToAction("SplitPdf");
        }
        
        #endregion

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
