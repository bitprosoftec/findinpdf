﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ITextnSpire.Models
{
    public class PDFFilenText
    {
        [NotMapped]
        public IFormFile PDFFIle { get; set; }

        public string PDFFileName { get; set; }

        public string SearchText { get; set; }

        public int PageNumber { get; set; }

        public List<String> paths { get; set; }
    }
}
